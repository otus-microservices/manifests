# install:

istioctl install --set profile=default -y

# apply monitoring
kubectl apply -f monitoring.yaml

# create gateway
kubectl apply -f gateway.yaml