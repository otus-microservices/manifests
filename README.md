## Requirements

- Minikube 1.26.1
- Kubectl 1.24.3
- Istioctl 1.13.0
- Heml 3.9.3

## Install steps

0. Add to /etc/hosts

```
${INGRESS_IP} arch.homework
${INGRESS_IP} grafana.arch.homework
${INGRESS_IP} prom.arch.homework
${INGRESS_IP} kiali.arch.homework
```

1. Install istio

```
istioctl install --set profile=default -y
```

2. Install monitoring

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
kubectl apply -f monitoring/namespaces.yaml
helm install kube-prometheus-stack --version="39.6.0" --namespace monitoring \
  --values prometheus-stack.yaml \
  prometheus-community/kube-prometheus-stack
```  

3. Create gateway

```
kubectl apply -f istio/gateway.yaml
```

4. Apply virtual services for monitoring

```
kubectl apply -f monitoring/vs-grafana.yaml
kubectl apply -f monitoring/vs-prom.yaml
```

5. Apply monitoring for istio

```
kubectl apply -f istio/monitoring.yaml
```

6. Install kiali

```
helm repo add kiali https://kiali.org/helm-charts
helm repo update
kubectl apply -f kiali/namespaces.yaml
helm install -n kiali-operator --version="1.54.0" kiali-operator kiali/kiali-operator
kubectl apply -f kiali/kiali.yaml
kubectl apply -f kiali/vc.yaml
```
