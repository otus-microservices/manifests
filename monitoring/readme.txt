# install:

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install kube-prometheus-stack --version="39.6.0" --namespace monitoring \
  --values prometheus-stack.yaml \
  prometheus-community/kube-prometheus-stack

# apply virtual service
kubectl apply -f virtual-service.yaml
